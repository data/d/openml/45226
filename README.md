# OpenML dataset: diabetes_7

https://www.openml.org/d/45226

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Diabetes Bayesian Network. Sample 7.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-verylarge.html#diabetes)

- Number of nodes: 413

- Number of arcs: 602

- Number of parameters: 429409

- Average Markov blanket size: 3.97

- Average degree: 2.92

- Maximum in-degree: 2

**Authors**: S. Andreassen, R. Hovorka, J. Benn, K. G. Olesen, and E. R. Carson.

**Please cite**: ([URL](https://vbn.aau.dk/en/publications/a-model-based-approach-to-insulin-adjustment)): S. Andreassen, R. Hovorka, J. Benn, K. G. Olesen, and E. R. Carson. A Model-based Approach to Insulin Adjustment. In Proceedings of the 3rd Conference on Artificial Intelligence in Medicine, pages 239-248. Springer-Verlag, 1991.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45226) of an [OpenML dataset](https://www.openml.org/d/45226). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45226/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45226/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45226/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

